<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">

    <!-- Generated: 2018-04-16 09:29:05 +0200 -->
    <title>Kirim pesan rahasia ke <?php echo $user ?></title>
    <?php $this->load->view('parts/head'); ?>
    <style>
        .ck-editor__editable {
            min-height: 300px;
        }
    </style>
   </head>
   <body>
     
    <?php $this->load->view('parts/header') ?>
    
    <main class="profile-page">
    <section class="section-profile-cover section-shaped my-0">
      <!-- Circles background -->
      <div class="shape shape-style-1 shape-primary alpha-4">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>
      <!-- SVG separator -->
    </section>
    <section class="section">
      <div class="container">
        <div class="card card-send mt--400">
          <div class="px-4">
            <?php if($this->session->flashdata('captcha')){  ?>
                <div class="alert alert-danger alert-dismissible fade show" id="success-alert" style="top:-400px;" role="alert">
                    <strong>Oops</strong> <?php echo $this->session->flashdata('captcha'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php } ?>
            <div class="row mt-3">
                <div class="col-md-1 col-sm-2 col-3">
                    <img src="<?php echo $img ?>" class="rounded-circle" style="width:100%">
                </div>
                <div class="col-md-11 col-sm-10 col-9">
                    <p class="mb-0">Kirim PesanRahasia ke</p>
                    <h5 class="mt-0"><?php echo $fullname ?></h5>
                </div>
            </div>
            <hr>
            <div class="mb-5">
                <form action="<?php echo base_url('chat/send_to_user'); ?>" method="post" />
                    <input type="hidden" name="penerima" value="<?php echo $id ?>" />
                    <input type="hidden" name="url_profil" value="<?php echo $this->uri->segment(1) ?>" />
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Jenis Pesan</label><br>
                                <select name="jenis" id="" class="form-control" required>
                                    <option value="">Pilih Jenis Pesan</option>
                                    <?php foreach($this->db->get('kategori_pesan')->result() as $ktg): ?>
                                    <option value="<?php echo $ktg->id ?>"><?php echo $ktg->kategori ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label class="mr-2">Kirim Sebagai : </label> <br class="content-mobile">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="anonim" name="anonim" value="yes" class="custom-control-input" checked onclick="showForm()">
                                <label class="custom-control-label" for="anonim">Anonim</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="identitas" name="anonim" value="no" class="custom-control-input" onclick="showForm()">
                                <label class="custom-control-label" for="identitas">Identitas / Samaran</label>
                            </div>
                            <div id="formPengirimShow" style="display:none">
                                <div class="content-mobile mt-3"></div>
                                <input type="text" name="pengirim_name" class="form-control" placeholder="Masukan identitas / nama samaran" minlenght="3" maxlenght="25">
                                <small>Contoh: Pemuja Rahasia</small>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="content-mobile mt-4"></div>
                            <div class="form-group">
                                <label>
                                    Apakah anda ingin menerima balasan dari <?php echo $fullname ?> secara rahasia? 
                                    <small><?php echo $fullname ?> Tidak akan pernah tau siapa anda dan apa email anda, pesan akan dikirim secara otomatis melalui sistem</small>
                                </label><br>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="no" name="balas" value="no" class="custom-control-input" onclick="showEm()" checked>
                                    <label class="custom-control-label" for="no">Tidak</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="ya" name="balas" value="yes" class="custom-control-input" onclick="showEm()">
                                    <label class="custom-control-label" for="ya">Iya</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6" id="showEmailForm" style="display:none">
                            <div class="form-group">
                                <label class="content-desktop">Masukan Email anda</label>
                                <input type="email" id="emailReq" class="form-control" name="email_pengirim" placeholder="Email Anda">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Tulis Pesan anda</label>
                                <textarea name="pesan" placeholder="Tulis pesan disini..." id="editor"></textarea>
                            </div>
                        </div>

                        <div class="col-md-12 mb-3">
                            <label>Pastikan anda bukan robot</label>
                            <?php echo $widget;?>
                            <?php echo $script;?>
                        </div>

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success form-control">Kirim</button>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
    
  <!-- Argon Scripts -->
  <!-- Core -->
  <?php $this->load->view('parts/script'); ?>
  <script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>
  <script>
    new ClipboardJS('#buttonCopy');

    function showForm() {
        var checkBox = document.getElementById("identitas");
        var showForm = document.getElementById("formPengirimShow");

        if (checkBox.checked == true){
            showForm.style.display = "block";
        } else {
            showForm.style.display = "none";
        }
    }

    function showEm() {
        var checkBox = document.getElementById("ya");
        var showForm = document.getElementById("showEmailForm");
        var emailReq = document.getElementById("showEmailemailReqForm");

        if (checkBox.checked == true){
            showForm.style.display = "block";
            emailReq.required = true;
        } else {
            showForm.style.display = "none";
        }
    }
    
</script>
<script>
    ClassicEditor
    .create( document.querySelector( '#editor' ), {
        toolbar: ['bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ]
    } )
    .catch( error => {
        console.log( error );
    } );
</script>
</body>
</html>


<header class="header-global">
    <nav id="navbar-main" class="navbar navbar-main navbar-expand-lg navbar-transparent navbar-light headroom">
      <div class="container">
        <a class="navbar-brand mr-lg-5 content-desktop" href="<?php echo base_url() ?>" />
          <?php if($this->session->userdata('logged_in') == true) { ?>
          <img src="<?php echo base_url('assets/images/rhs.png') ?>" >
          <?php }else{ ?>
          <img src="<?php echo base_url('assets/images/logo_white.png') ?>" style="width:150px; height:auto;">
          <?php } ?>
        </a>
        <a class="navbar-brand mr-lg-5 content-mobile" href="<?php echo base_url() ?>" />
          <img src="<?php echo base_url('assets/images/rhs.png') ?>" >
        </a>
        <form action="">
          
          <div class="input-group search-user">
            <!-- Autocomplete input field -->
            <input class="form-control" id="searchInput" name="q" placeholder="Cari seseorang..." autocomplete="off">
            <!-- Hidden input for user ID -->  
            <input type="hidden" id="userID" name="userID" value=""/>
            <div class="input-group-append">
              <button class="btn btn-primary" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
            </div>
          </div>
          
        </form>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse" id="navbar_global">
          <div class="navbar-collapse-header">
            <div class="row">
              <div class="col-6 collapse-brand">
                <a class="navbar-brand mr-lg-5" href="<?php echo base_url() ?>" />
                  <img src="<?php echo base_url('assets/images/logo.png') ?>" style="width:170px">
                </a>
              </div>
              <div class="col-6 collapse-close">
                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
                  <span></span>
                  <span></span>
                </button>
              </div>
            </div>
          </div>
          <ul class="navbar-nav navbar-nav-hover align-items-lg-center">
            <?php if($this->session->userdata('logged_in') == true) { ?>
            <!-- 
            <li class="nav-link">
              <a href="#">Pengaturan</a>
            </li>
            <li class="nav-link">
              <a href="#">Pesan Terkirim</a>
            </li> -->
            <li class="nav-link">
              <a href="<?php echo base_url('feedback') ?>"><i class="far fa-comments"></i> Beri Feedback</a></a>
            </li>
            <li class="nav-link">
              <a href="<?php echo base_url('ubah-username') ?>">Ubah Username</a></a>
            </li>
            <li class="nav-link">
              <a href="<?php echo base_url('dasbor/logout') ?>">Keluar</a></a>
            </li>
            <?php } ?>
          </ul>
          <ul class="navbar-nav align-items-lg-center ml-lg-auto">
            
            <li class="nav-item d-none d-lg-block ml-lg-4">
              <?php if($this->session->userdata('logged_in') == true){ ?>
                <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <div class="media align-items-center">
                    <span class="avatar avatar-sm rounded-circle">
                      <img alt="Image placeholder" src="<?php echo $this->session->userdata('img') ?>">
                    </span>
                    <div class="media-body ml-2 d-none d-lg-block">
                      <span class="mb-0 text-white font-weight-bold"><?php echo $this->session->userdata('first_name') ?></span>
                    </div>
                  </div>
                </a>
              <?php }else{ ?>
                <a href="<?php echo $this->google->get_login_url(); ?>" class="btn btn-neutral btn-icon">
                  <span class="btn-inner--icon">
                    <img src="<?php echo base_url('assets/images/google.png') ?>" style="width:18px;" class="mr-3" alt="">
                  </span>
                  <span class="nav-link-inner--text" style="color: #FF5733">Masuk dengan Google</span>
                </a>
              <?php } ?>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>
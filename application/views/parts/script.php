<!-- Argon Scripts -->
<!-- Core -->
<script src="<?php echo base_url() ?>assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Optional JS -->
<script src="<?php echo base_url() ?>assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<!-- Argon JS -->
<script src="<?php echo base_url() ?>assets/js/argon.js?v=1.0.0"></script>

<!-- SweetAlert JS -->
<script src="<?php echo base_url() ?>assets/vendor/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/sweetalert/dialogs.js"></script>

<!-- clipboard.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script>

<!-- DataTable -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

<!-- JQuery UI autocomplete -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    $(document).ready(function() {
        $('.table').DataTable({
            "ordering": false
        });
    } );
</script>

<script type="text/javascript">
    var date = new Date();
    var currentMonth = date.getMonth(); // current month
    var currentDate = date.getDate(); // current date
    var currentYear = date.getFullYear(); //this year
    $('.input-daterange').each(function() {
        $(this).datepicker({
            format: 'dd/mm/yyyy',
            minDate: new Date(currentYear, currentMonth, currentDate)
        });
    });
    
    $('.datepicker2').datepicker({
        format: 'dd/mm/yyyy',
        startDate: '0d'
    });
    

    $(document).ready(function(){
        $('#status').change(function(){
            if(this.checked)
                formDate.style.display = "";
            else
                formDate.style.display = "none";
        });
    });

    function showDate()
    {
        var status = document.getElementById('status');
        var formDate = document.getElementById('formDate');
        if(status.value == "on"){
            formDate.style.display = "";
        }else{
            formDate.style.display = "none";
        }
    }


</script>
<script>
    /*
    $(function() {
        $("#autosuggest").autocomplete({
            source: "<?php echo base_url('landing/cariuser'); ?>",
            select: function( event, ui ) {
                event.preventDefault();
                $("#autosuggest").val(ui.item.id);
            },
            minLength: 3
        });
    }); */

    $(document).ready(function(){
        $("#searchInput").autocomplete({
            source: "<?php echo base_url('landing/cariuser'); ?>",
            minLength: 3,
            select: function(event, ui) {
                $("#searchInput").val(ui.item.value);
                $("#userID").val(ui.item.id);
            }
        }).data("ui-autocomplete")._renderItem = function( ul, item ) {
        return $( "<li class='ui-autocomplete-row'></li>" )
            .data( "item.autocomplete", item )
            .append( item.label )
            .appendTo( ul );
        };
    });
</script>
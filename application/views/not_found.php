<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">

    <!-- Generated: 2018-04-16 09:29:05 +0200 -->
    <title>Kirim pesan rahasia</title>
    <?php $this->load->view('parts/head'); ?>
    <style>
        .ck-editor__editable {
            min-height: 300px;
        }
    </style>
   </head>
   <body>
     
    <?php $this->load->view('parts/header') ?>
    
    <main class="profile-page">
    <section class="section-profile-cover section-shaped my-0">
      <!-- Circles background -->
      <div class="shape shape-style-1 shape-primary alpha-4">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>
      <!-- SVG separator -->
    </section>
    <section class="section">
      <div class="container">
        <div class="card card-send mt--400">
          
            <div class="card-body">
                <div class="mb-5">
                    <h3 class="text-center">
                        Oops, halaman yang anda cari tidak ditemukan
                    </h3>

                    <div class="text-center mt-5">
                        <?php if($this->session->userdata('logged_in') == true) { ?>
                        <a href="<?php base_url() ?>" class="btn btn-secondary">Masuk ke Halaman Saya</a>
                        <?php } else {  ?>
                        <a href="<?php echo $this->google->get_login_url(); ?>" class="btn ml-3" style="background-color:#FF5733">
                            <span class="btn-inner--icon">
                                <i class="fab fa-google mr-3 text-white"></i>
                            </span>
                            <span class="nav-link-inner--text" style="color: #fff">Mulai Sekarang</span>
                        </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
    
  <!-- Argon Scripts -->
  <!-- Core -->
  <?php $this->load->view('parts/script'); ?>
  <script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>
  <script>
    new ClipboardJS('#buttonCopy');

    function showForm() {
        var checkBox = document.getElementById("identitas");
        var showForm = document.getElementById("formPengirimShow");

        if (checkBox.checked == true){
            showForm.style.display = "block";
        } else {
            showForm.style.display = "none";
        }
    }

    function showEm() {
        var checkBox = document.getElementById("ya");
        var showForm = document.getElementById("showEmailForm");
        var emailReq = document.getElementById("showEmailemailReqForm");

        if (checkBox.checked == true){
            showForm.style.display = "block";
            emailReq.required = true;
        } else {
            showForm.style.display = "none";
        }
    }
    
</script>
<script>
    ClassicEditor
    .create( document.querySelector( '#editor' ), {
        toolbar: ['bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ]
    } )
    .catch( error => {
        console.log( error );
    } );
</script>
</body>
</html>


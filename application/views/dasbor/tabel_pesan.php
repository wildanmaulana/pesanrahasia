<?php if($table == "all"){
    $id = $this->session->userdata('id');
    $query = "SELECT *,p.id as id_pesan FROM pesan p LEFT JOIN kategori_pesan k ON(p.kategori = k.id) WHERE penerima = '$id' ORDER BY p.tanggal DESC";
}else{
    $query = "SELECT *,p.id as id_pesan FROM pesan p LEFT JOIN kategori_pesan k ON(p.kategori = k.id) WHERE penerima = '$id' AND p.kategori = '$table' ORDER BY p.tanggal DESC";
} ?>
<div class="content-desktop mt-3">
    <table class="table table-striped" id="dataTable" style="width:100%;">
        <thead>
            <td>Tanggal</td>
            <td>Isi Pesan</td>
            <td>Opsi</td>
        </thead>
        <tbody>
        <?php foreach($this->db->query($query)->result() as $i => $tbl): ?>
        <tr>
            <td>
                <?php echo $this->configuration->get_date($tbl->tanggal); ?><br>
                <small><?php echo $tbl->pengirim; ?></small>
            </td>
            <td><a href="<?php echo base_url('chat/').$tbl->id_pesan ?>" style="color:#474747">
                <?php echo $this->configuration->get_kat_pesan($tbl->kategori); ?>
                <?php echo substr(strip_tags($this->encryption->decrypt($tbl->pesan)),0,60); ?></a>
            </td>
            <td>
                <?php if($tbl->balas == true){ ?>
                    <a href="<?php echo base_url('chat/').$tbl->id_pesan ?>/reply" class="btn btn-sm btn-success">Balas Pesan</a>
                <?php } ?>
            </td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div class="content-mobile mt-3">
    <table class="table table-striped table-sm" id="dataTable">
        <thead>
            <th>Pesan</th>
        </thead>
        <tbody>
        <?php foreach($this->db->query($query)->result() as $i => $tbl): ?>
        <tr>
            <td>
            <a href="<?php echo base_url('chat/').$tbl->id_pesan ?>" />
                <?php echo $this->configuration->get_kat_pesan($tbl->kategori); ?>   <?php echo $tbl->pengirim; ?> <span class="float-right"><?php echo $this->configuration->get_date($tbl->tanggal); ?></span><br>
                <p class="mt-2 mb-0"><?php echo substr(strip_tags($this->encryption->decrypt($tbl->pesan)),0,40); ?></p>
                <?php if($tbl->balas == true){ ?>
                <span class="badge badge-success float-right"><i class="fas fa-exclamation-circle mr-2"></i> balas pesan ini</span>
                <?php } ?>
            </a>
            </td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>


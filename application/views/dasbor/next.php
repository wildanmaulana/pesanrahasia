<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">

    <!-- Generated: 2018-04-16 09:29:05 +0200 -->
    <title>PesanRahasia | Kirim dan Curhat dengan Rahasia</title>
    <?php $this->load->view('parts/head'); ?>
   </head>
   <body class="bg-primary">
     
    <div class="content-desktop">
      <?php $this->load->view('parts/header') ?>
    </div>
    <div class="main-content">
    <!-- Page content -->
    <main>
        <section class="section section-shaped section-lg">
            <div class="shape shape-style-1 bg-gradient-default">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
            <div class="container pt-lg-md">
                <div class="row justify-content-center">
                    <div class="col-lg-5">
                        <div class="card bg-secondary shadow border-0 mt-5">
                            <div class="card-body px-lg-5 py-lg-5">
                                <div class="text-center text-muted mb-4">
                                <h2>Satu Langkah lagi anda dapat menerima pesan rahasia</h2>
                                </div>
                                <form role="form" id="formUser" method="post" action="<?php echo base_url('dasbor/nextstep') ?>" >
                                    <div class="form-group">
                                        <label>Masukkan Username</label>
                                        <input class="form-control" id="username" name="username" placeholder="Masukkan Username" type="text" required>
                                    </div>
                                    <small>Karakter diizinkan (A-Z,a-z,0-9) dan tanpa spasi</small>
                                    
                                    
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-primary my-4 form-control">Lanjutkan</button>
                                    </div>
                                </form>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    </div>
    <!-- Argon Scripts -->
    <!-- Core -->
    <?php $this->load->view('parts/script'); ?>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    
    <script>
        $(document).ready(function () {
            jQuery.validator.addMethod("usrregex", function(value, element){
                if (/[^a-zA-Z0-9]/.test(value)) {
                    return false;
                } else {
                    return true;
                };
            }, "Oops, Gunakan karakter yang benar.. "); 

            $("#formUser").validate({
                rules: {
                    username: {
                        required: true,
                        minlength: 3,
                        maxlength: 15,
                        usrregex: true,
                        remote: {
                            url: "<?php echo base_url('dasbor/cekusername') ?>",
                            type: "post",
                            data: {
                                username: function() {
                                    return $( "#username" ).val();
                                }
                            }
                        }
                    }
                },
                messages: {
                    username: {
                        remote: "Oops, username sudah terpakai",
                        regex: "Oops, harap menggunakan karakter yang benar"
                    }
                }
            });
        });
    </script>
</body>
</html>


<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">

    <!-- Generated: 2018-04-16 09:29:05 +0200 -->
    <title>PesanRahasia | Kirim dan Curhat dengan Rahasia</title>
    <?php $this->load->view('parts/head'); ?>
    <style>
        .ck-editor__editable {
            min-height: 300px;
        }
    </style>
   </head>
   <body>
     
    <?php $this->load->view('parts/header') ?>
    
    <main class="profile-page">
        <section class="section-profile-cover section-shaped my-0">
        <!-- Circles background -->
        <div class="shape shape-style-1 shape-primary alpha-4">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
        <!-- SVG separator -->
        </section>
        <section class="section">
            <div class="container">
                <?php foreach($getMess->result() as $tbl): ?>
                
                <div class="card card-profile shadow mt--400">
                    
                    <div class="card-header">
                        <h5 class="float-left">Ubah Profil</h5>
                    </div>
                    <div class="card-body">
                        
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </section>
    </main>
    
    <!-- Argon Scripts -->
    <!-- Core -->
    <?php $this->load->view('parts/script'); ?>
    <script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
        .create( document.querySelector( '#editor' ), {
            toolbar: ['bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ]
        } )
        .catch( error => {
            console.log( error );
        } );

        $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#success-alert").slideUp(500);
        });
    </script>
</body>
</html>


<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">

    <!-- Generated: 2018-04-16 09:29:05 +0200 -->
    <title>PesanRahasia | Kirim dan Curhat dengan Rahasia</title>
    <?php $this->load->view('parts/head'); ?>
   </head>
   <body>
     
    <?php $this->load->view('parts/header') ?>
    
    <main class="profile-page">

    <section class="section-profile-cover section-shaped my-0">
      <!-- Circles background -->
      <div class="shape shape-style-1 shape-primary alpha-4">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>
      <!-- SVG separator -->
    </section>
    <section class="section">

      <?php if($this->session->flashdata('notif_username')) { ?>
        <div class="myAlert-top alert alert-success" id="success-alert">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Horee!</strong> <?php echo $this->session->flashdata('notif_username'); ?>
        </div>
      <?php } ?>
      
      <div class="container">
        <div class="card card-profile shadow mt--400">
          <div class="px-4">
            <div class="row justify-content-center">
              <div class="col-lg-3 order-lg-2">
                <div class="card-profile-image">
                  <a href="#">
                    <img src="<?php echo $this->session->userdata('img') ?>" class="rounded-circle">
                  </a>
                </div>
              </div>
              <div class="col-lg-4 order-lg-3 text-lg-right align-self-lg-center">
                <div class="card-profile-actions py-4 mt-lg-0">
                  <div class="input-group mb-3">
                    <input type="text" id="myurl" class="form-control" value="https://pesanrahasia.site/<?php echo $username; ?>" style="height:35px;">
                    <div class="input-group-append">
                      <button class="btn btn-sm btn-primary" id="buttonCopy" type="button" data-clipboard-target="#myurl">salin</button>
                    </div>
                  </div>
                  <small>untuk menerima pesan rahasia, copy link diatas dan bagikan link kepada siapapun</small>
                  <div class="share-sec">
                    <!-- Facebook Share -->
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.2&appId=295931487722854&autoLogAppEvents=1';
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>
                    <div class="fb-share-button" data-href="https://pesanrahasia.site/<?php echo $username; ?>" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fpesanrahasia.site%2F<?php echo $username ?>&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Bagikan</a></div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 order-lg-1 col-liked">
                <div class="card-profile-stats d-flex justify-content-center">
                  <div>
                    <span class="heading"><?php echo $this->db->query("SELECT * FROM pesan WHERE penerima = '$id' AND kategori = '1'")->num_rows(); ?></span>
                    <span class="description">Pesan</span>
                  </div>
                  <div>
                    <span class="heading"><?php echo $this->db->query("SELECT * FROM pesan WHERE penerima = '$id' AND kategori = '2'")->num_rows(); ?></span>
                    <span class="description">Curhat</span>
                  </div>
                  <div>
                    <span class="heading"><?php echo $this->db->query("SELECT * FROM pesan WHERE penerima = '$id' AND kategori = '3'")->num_rows(); ?></span>
                    <span class="description">Kritikan</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="text-center mt-3">
              <h3 class="mb-0" style="line-height: 1em">
                <?php echo $first_name.' '.$last_name ?><br>
                <span class="font-weight-light text-center" style="font-size:12pt;">id: <?php echo $username; ?> <a href="<?php echo base_url('ubah-username') ?>">(ubah)</a></span>
              </h3>
            </div>
            <hr>
            <div class="mb-5">
              <h5>Pesan Rahasia</h5>
              
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">All</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Pesan</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Curhat</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="contact-tab" data-toggle="tab" href="#kritik" role="tab" aria-controls="kritik" aria-selected="false">Kritik</a>
                </li>
              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                  <?php
                  $data['table'] = 'all';
                  $this->load->view('dasbor/tabel_pesan',$data);
                  ?>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                  <?php
                  $data['table'] = '1';
                  $this->load->view('dasbor/tabel_pesan',$data);
                  ?>
                </div>
                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                  <?php
                  $data['table'] = '2';
                  $this->load->view('dasbor/tabel_pesan',$data);
                  ?>  
                </div>
                <div class="tab-pane fade" id="kritik" role="tabpanel" aria-labelledby="kritik-tab">
                  <?php
                  $data['table'] = '3';
                  $this->load->view('dasbor/tabel_pesan',$data);
                  ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
    
  <!-- Argon Scripts -->
  <!-- Core -->
  <?php $this->load->view('parts/script'); ?>
  <script>
    new ClipboardJS('#buttonCopy');

    $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#success-alert").slideUp(500);
    });
</script>
</body>
</html>


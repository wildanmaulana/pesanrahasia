<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">

    <!-- Generated: 2018-04-16 09:29:05 +0200 -->
    <title>PesanRahasia | Kirim dan Curhat dengan Rahasia</title>
    <?php $this->load->view('parts/head'); ?>
    <style>
        .ck-editor__editable {
            min-height: 300px;
        }
    </style>
   </head>
   <body>
     
    <?php $this->load->view('parts/header') ?>
    
    <main class="profile-page">
        <section class="section-profile-cover section-shaped my-0">
        <!-- Circles background -->
        <div class="shape shape-style-1 shape-primary alpha-4">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
        <!-- SVG separator -->
        </section>
        <section class="section">
            <div class="container">
                <?php foreach($getMess->result() as $tbl): ?>
                <?php if($this->session->flashdata('pesan_terkirim')) { ?>
                    <div class="alert alert-success alert-dismissible fade show" id="success-alert" style="top:-400px;" role="alert">
                        <strong>Horee...</strong> <?php echo $this->session->flashdata('pesan_terkirim'); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>
                
                <div class="card card-profile shadow mt--400">
                    
                    <div class="card-header">
                        <h6 class="float-left">Pesan dari <?php echo $tbl->pengirim ?></h6>
                        <?php if($tbl->balas == true){ ?>
                            <a href="<?php echo base_url('chat/').$this->uri->segment(2); ?>/reply" class="btn btn-sm btn-success float-right" style="display:flex">Balas Pesan</a>
                        <?php } ?>
                    </div>
                    <div class="card-body">
                        <button class="btn btn-sm btn-primary" onclick="window.history.back();"><i class="fas fa-arrow-left"></i></button>
                        <?php echo $this->configuration->get_kat_pesan($tbl->kategori); ?> 
                        <span class="float-right" style="font-size:10pt; margin-top:5px;"><?php echo $this->configuration->get_full_date($tbl->tanggal); ?></span>
                        <hr style="margin-top:16px; margin-bottom:20px;">
                        <?php echo $this->encryption->decrypt($tbl->pesan); ?>
                        <?php if($tbl->balas == true){ ?>
                            <a href="<?php echo base_url('chat/').$this->uri->segment(2); ?>/reply" class="btn btn-sm btn-success float-right" style="display:flex">Balas Pesan</a>
                        <?php } ?>

                        <!-- Balas Pesan -->
                        <?php if($this->uri->segment(3) == "reply"){ ?>
                        <hr>
                        <h5>Balas pesan ke <?php echo $tbl->pengirim ?></h5>
                        <form action="<?php echo base_url('chat/balas_pesan'); ?>" method="post">
                            <input type="hidden" name="id_pesan" value="<?php echo $tbl->id_pesan; ?>" />
                            <input type="hidden" name="email" value="<?php echo $tbl->email_balas; ?>" />
                            <div class="form-group">
                                <label>Tulis balasan anda</label>
                                <textarea name="pesan" id="editor" cols="30" rows="10"></textarea>
                            </div>
                            <button type="submit" class="btn btn-success float-right">Kirim Pesan</button>
                        </form>
                        <?php } ?>
                        
                        <!-- Kondisi jika pesan memiliki balasan -->
                        <?php
                        $psn_balasan = $this->db->query("SELECT * FROM pesan_balasan WHERE id_pesan = '$tbl->id_pesan'");
                        if($psn_balasan->num_rows() > 0){ ?>
                        <hr>
                        <?php foreach($psn_balasan->result() as $psn): ?>
                        <h5 class="mb-0">Pesan Balasan </h5>
                        <small><?php echo $this->configuration->get_full_date($psn->tanggal); ?></small>
                        <p><?php echo $psn->pesan_balasan; ?></p>
                        <?php endforeach; }  ?>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </section>
    </main>
    
    <!-- Argon Scripts -->
    <!-- Core -->
    <?php $this->load->view('parts/script'); ?>
    <script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
        .create( document.querySelector( '#editor' ), {
            toolbar: ['bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ]
        } )
        .catch( error => {
            console.log( error );
        } );

        $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#success-alert").slideUp(500);
        });
    </script>
</body>
</html>


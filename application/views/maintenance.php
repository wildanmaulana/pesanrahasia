<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <!-- Global site tag (gtag.js) - Google Analytics -->

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">

    <!-- Generated: 2018-04-16 09:29:05 +0200 -->
    <title>UNDER MANITENANCE</title>
    <?php $this->load->view('parts/head'); ?>
   </head>
   <body class="bg-primary">
    
   <?php $this->load->view('parts/header') ?>
    <div class="main-content">
    <!-- Page content -->
    <div class="container">
      <div class="row container-main">
        <div class="col-md-8 ">
          <h1 class="text-white">Oops, Website sedang dalam perbaikan</h1>
          <p>
            Website PesanRahasia sedang diperbaiki, kami akan segera kembali beberapa saat lagi<br><br>
            Mohon Maaf atas ketidaknyamanaan.
          </p>
        <div class="col-md-4">
          <img src="<?php echo base_url('assets/images/mail.png') ?>" style="width:100%;" alt="">
        </div>
      </div>
    </div>
  </div>

  <!-- Argon Scripts -->
  <!-- Core -->
  <?php $this->load->view('parts/script'); ?>
</body>
</html>


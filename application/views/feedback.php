<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">

    <!-- Generated: 2018-04-16 09:29:05 +0200 -->
    <title>Beri kami feedback anda | PesanRahasia.site</title>
    <?php $this->load->view('parts/head'); ?>
    <style>
        .ck-editor__editable {
            min-height: 300px;
        }
    </style>
   </head>
   <body>
     
    <?php $this->load->view('parts/header') ?>
    
    <main class="profile-page">
    <section class="section-profile-cover section-shaped my-0">
      <!-- Circles background -->
      <div class="shape shape-style-1 shape-primary alpha-4">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>
      <!-- SVG separator -->
    </section>
    <section class="section">
      <div class="container">
        <div class="card card-send mt--400">
            <div class="card-header">
                <h5>Beri Feedback</h5>
            </div>
          <div class="px-4">
            
            
            <hr>
            <div class="mb-5">
                <form action="<?php echo base_url('chat/send_feedback'); ?>" method="post" />
                    <input type="hidden" name="url_profil" value="<?php echo $this->uri->segment(1) ?>" />
                    <div class="row">
                        

                        <div class="col-md-6">
                            <label class="mr-2">Kirim Sebagai : </label> <br class="content-mobile">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="anonim" name="anonim" value="yes" class="custom-control-input" checked onclick="showForm()">
                                <label class="custom-control-label" for="anonim">Anonim</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="identitas" name="anonim" value="no" class="custom-control-input" onclick="showForm()">
                                <label class="custom-control-label" for="identitas">Identitas / Samaran</label>
                            </div>
                            <div id="formPengirimShow" style="display:none">
                                <div class="content-mobile mt-3"></div>
                                <input type="text" name="pengirim_name" class="form-control" placeholder="Masukan identitas / nama samaran" minlenght="3" maxlenght="25">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="content-desktop">Masukan Email anda (opsional)</label>
                                <input type="email" id="emailReq" class="form-control" name="email" placeholder="Email Anda">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Tulis Pesan anda</label>
                                <textarea name="pesan" placeholder="Tulis pesan disini..." id="editor"></textarea>
                            </div>
                        </div>

                        <div class="col-md-12 mb-3">
                            <label>Pastikan anda bukan robot</label>
                            <?php echo $widget;?>
                            <?php echo $script;?>
                        </div>

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success form-control">Kirim</button>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
    
  <!-- Argon Scripts -->
  <!-- Core -->
  <?php $this->load->view('parts/script'); ?>
  <script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>
  <script>
    new ClipboardJS('#buttonCopy');

    function showForm() {
        var checkBox = document.getElementById("identitas");
        var showForm = document.getElementById("formPengirimShow");

        if (checkBox.checked == true){
            showForm.style.display = "block";
        } else {
            showForm.style.display = "none";
        }
    }

    function showEm() {
        var checkBox = document.getElementById("ya");
        var showForm = document.getElementById("showEmailForm");
        var emailReq = document.getElementById("showEmailemailReqForm");

        if (checkBox.checked == true){
            showForm.style.display = "block";
            emailReq.required = true;
        } else {
            showForm.style.display = "none";
        }
    }
    
</script>
<script>
    ClassicEditor
    .create( document.querySelector( '#editor' ), {
        toolbar: ['bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ]
    } )
    .catch( error => {
        console.log( error );
    } );
</script>
</body>
</html>


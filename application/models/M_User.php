<?php 

class M_User extends CI_Model{	
	function cek_jadwal($id_pengajar)
	{
		return $this->db->query("SELECT * FROM pengajar WHERE waktu_kosong is null AND id_pengajar = '$id_pengajar'");
	}
	function ambil($table,$where)
	{
		return $this->db->get_where($table,$where);
	}
	function edit_ngajar($user, $data_ngajar)
    {
        $this->db->where('id_pengajar',$user);
        $this->db->update('pengajar', $data_ngajar);
    }
    function ambil_data($user)
    {
    	return $this->db->get_where('pengajar',$user);
    }
	function data_fakultas($where)
	{
		return $this->db->get_where('fakultas',$where);
	}
	function get_jurusan()
	{
		$query=$this->db->query("SELECT * FROM jurusan WHERE no_jurusan != 0 AND no_fakultas = $_GET[q]");
        return $query->result();
	}
	function tempat_lahir_autocomplete($where,$kode)
	{
		$this->db->where($where);
		$this->db->like('lokasi_nama',$kode);
	  	$query=$this->db->get('inf_lokasi');
	  	return $query->result();
	}

	public function get_provinsi()
	{
		$query=$this->db->query("SELECT * FROM inf_lokasi WHERE lokasi_kabupatenkota = 0 AND lokasi_kecamatan = 0 AND lokasi_kelurahan = 0");
        return $query->result();
	}
	public function get_kota()
	{
		$query=$this->db->query("SELECT * FROM inf_lokasi where lokasi_propinsi=$_GET[q] and lokasi_kecamatan=0 and lokasi_kelurahan=0 and lokasi_kabupatenkota!=0 order by lokasi_nama");
		return $query->result();
	}
	public function get_kec()
	{
		$query=$this->db->query("SELECT * FROM inf_lokasi where lokasi_propinsi=$_GET[prop] and lokasi_kecamatan!=0 and lokasi_kelurahan=0 and lokasi_kabupatenkota=$_GET[kec]");
		return $query->result();
	}
	public function get_kel()
	{
		$query=$this->db->query("SELECT * FROM inf_lokasi where lokasi_propinsi=$_GET[prop] and lokasi_kecamatan=$_GET[kel] and lokasi_kelurahan!=0 and lokasi_kabupatenkota=$_GET[kec]");
		return $query->result();
	}
	public function ambil_data_fakultas()
	{
		$query = $this->db->query("SELECT * FROM fakultas WHERE no_jurusan = 0");
		return $query->result();
	}
	public function getjurusan()
	{
		$query=$this->db->query("SELECT * FROM fakultas where no_fakultas=$_GET[q]");
		return $query->result();
	}
	public function editProfil($id_pengajar,$data_profil_edit)
	{
		$this->db->where('id_pengajar',$id_pengajar);
		$this->db->update('pengajar',$data_profil_edit);
	}
	public function gantiPasswordDefault($id,$passwordbaru)
	{
		$this->db->where('username',$id);
		$this->db->update('user',$passwordbaru);
	}	
}
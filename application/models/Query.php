<?php 

class Query extends CI_Model{

	function get_data($table,$where)
	{
		return $this->db->get_where($table,$where);
	}

	function insert_data($table, $data)
	{
		$this->db->insert($table, $data);
	}
	function hapus_data($table, $where)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}

	function update_data($table, $where, $data)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	function data_no_induk($id_tpa)
	{
		return $this->db->query("SELECT * FROM santri WHERE tpa = '$id_tpa' ORDER BY induk_santri ASC");
	}

	// HITUNG 
	function jml_data($dt)
	{
		return $this->db->query("SELECT * FROM $dt")->num_rows();
	}

	// ANBIM 
	function anbim_all()
	{
		return $this->db->query("SELECT * FROM anbim an LEFT JOIN indukAnbim i ON(an.idIndukAnbim = i.id) LEFT JOIN riwayatPendidikan r ON(i.id = r.idIndukAnbim) LEFT JOIN organisasi o ON(i.id = o.idIndukAnbim) LEFT JOIN prestasi p ON(i.id = o.idIndukAnbim) LEFT JOIN pelatihan pl ON(i.id = pl.idIndukAnbim) LEFT JOIN beasiswaorbit b ON(i.id = b.idIndukAnbim) LEFT JOIN waliAngkat w ON(i.id = w.idIndukAnbim) GROUP BY i.id");
	}

	function anbim_where($where)
	{
		return $this->db->query("SELECT * FROM anbim an LEFT JOIN indukAnbim i ON(an.idIndukAnbim = i.id) LEFT JOIN riwayatPendidikan r ON(i.id = r.idIndukAnbim) LEFT JOIN organisasi o ON(i.id = o.idIndukAnbim) LEFT JOIN prestasi p ON(i.id = o.idIndukAnbim) LEFT JOIN pelatihan pl ON(i.id = pl.idIndukAnbim) LEFT JOIN beasiswaorbit b ON(i.id = b.idIndukAnbim) LEFT JOIN waliAngkat w ON(i.id = w.idIndukAnbim) WHERE $where GROUP BY i.id");
	}

	function anbim_profile($where)
	{
		return $this->db->query("SELECT * FROM anbim an LEFT JOIN indukAnbim i ON(an.idIndukAnbim = i.id) LEFT JOIN riwayatPendidikan r ON(i.id = r.idIndukAnbim) LEFT JOIN organisasi o ON(i.id = o.idIndukAnbim) LEFT JOIN prestasi p ON(i.id = o.idIndukAnbim) LEFT JOIN pelatihan pl ON(i.id = pl.idIndukAnbim) LEFT JOIN beasiswaorbit b ON(i.id = b.idIndukAnbim) LEFT JOIN waliAngkat w ON(i.id = w.idIndukAnbim) WHERE i.id = $where GROUP BY i.id");
	}

	function anbim_pendaftar($where)
	{
		return $this->db->query("SELECT * FROM pendaftar pd LEFT JOIN indukAnbim i ON(pd.idIndukAnbim = i.id) LEFT JOIN riwayatPendidikan r ON(i.id = r.idIndukAnbim) LEFT JOIN organisasi o ON(i.id = o.idIndukAnbim) LEFT JOIN prestasi p ON(i.id = o.idIndukAnbim) LEFT JOIN pelatihan pl ON(i.id = pl.idIndukAnbim) LEFT JOIN beasiswaorbit b ON(i.id = b.idIndukAnbim) LEFT JOIN waliAngkat w ON(i.id = w.idIndukAnbim) WHERE i.id = $where GROUP BY i.id");
	}
}
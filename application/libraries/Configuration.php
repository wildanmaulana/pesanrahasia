<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configuration {

	public function title_default()
	{
		$this->CI =& get_instance();
		$this->CI->load->model('Query');
		$this->CI->load->library('database');
		$w_title = array('name' => 'title_default');
		$q_title = $this->CI->Query->get_data('all_text',$w_title)->result();
		foreach($q_title as $title){
			$title_default	= $title->value;
		}
		return $title_default;
	}

	public function all_text($txt_)
	{
		$this->CI =& get_instance();
		$this->CI->load->model('Query');
		$w_title = array('name' => $txt_);
		$q_title = $this->CI->Query->get_data('all_text',$w_title)->result();
		foreach($q_title as $title){
			$title_default	= $title->value;
		}
		return $title_default;
	}

	public function get_text($text)
	{
		$this->CI =& get_instance();
		$CI =& get_instance();
		//$this->CI->load->library('session');

		if($this->CI->session->userdata('lang')){
			$lang = $this->CI->session->userdata('lang');
		}else{
			$lang = 'en';
		}

		$this->CI->load->model('Query');
		$w_text = array(
			'name' => $text,
			'lang' => $lang
		);

		$d_text = $this->CI->Query->get_data('all_text',$w_text)->result();

		foreach($d_text as $t){
			$text1 = $t->value;
		}
		return $text1;
	}

	public function hitung_umur($tgl)
	{
		if($tgl){
			list($day,$month,$year) = explode("/",$tgl);
		    $year_diff  = date("Y") - $year;
		    $month_diff = date("m") - $month;
		    $day_diff   = date("d") - $day;
		    
		    if ($month_diff < 0) $year_diff--;
		    elseif (($month_diff==0) && ($day_diff < 0)) $year_diff--;
		    
		    return $year_diff;
		}
	}

	

	public function send_email()
	{
		
	}

	public function set_new_password()
	{
		
	}

	public function get_date($tgl)
	{
		date_default_timezone_set("Asia/Jakarta");
		$date = new DateTime($tgl); 
		$dt = $date->getTimestamp(); 
		$dt_now = date('d');
		if(date('d',$dt) == $dt_now){
			$dt_show = date('H:i',$dt);
		}else{
			$dt_show = date('d M',$dt);
		}

		return $dt_show;
	}

	public function get_full_date($tgl)
	{
		date_default_timezone_set("Asia/Jakarta");
		$date = new DateTime($tgl); 
		$dt = $date->getTimestamp(); 
		$dt_show = date('d M Y - H:i',$dt);

		return $dt_show;
	}

	public function get_kat_pesan($kat)
	{
		if($kat == 'Pesan Rahasia'){
			$kat_ = '<span class="badge badge-primary mr-3">'.$kat.'</span> ';
		}elseif($kat == 'Curhat'){
			$kat_ = '<span class="badge badge-success mr-3">'.$kat.'</span> ';
		}elseif($kat == 'Kritikan'){
			$kat_ = '<span class="badge badge-warning mr-3">'.$kat.'</span> ';
		}else{
			$kat_ = '<span class="badge badge-warning mr-3">'.$kat.'</span> ';
		}

		return $kat_;
	}
}
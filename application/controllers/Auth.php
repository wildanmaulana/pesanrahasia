<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
        $this->load->library('Google');
        $this->load->library('encryption');
	}

	public function index()
	{
        //$google_data=$this->google->validate();
        $oauth_id = '108711921299526121277';

        date_default_timezone_set("Asia/Jakarta");
 
		$data_user = array(
            'oauth_id'      => $oauth_id,
            'nickname'      => '',
            'first_name'    => 'Wildan',
            'last_name'     => 'Maulana',
            'email'         => 'wildaanmaulana@gmail.com',
            'oauth_provider'        => 'google',
            'img'           => 'https://lh6.googleusercontent.com/-q1PhA2MqUl0/AAAAAAAAAAI/AAAAAAAALKo/gkcVFXOt4zg/photo.jpg?sz=800',
            'gender'        => 'male',
            'locale'        => 'ID',
            //'placeLived'    => !empty($google_data['placeLived'])?$google_data['placeLived']:'',
            'date_registered' => date('Y-m-d H:i:s')
        );

        $data_user_2 = array(
            'oauth_id'      => $oauth_id,
            'email'         => $google_data['email'],
            'oauth_provider'        => 'google',
            'img'           => 'https://lh6.googleusercontent.com/-q1PhA2MqUl0/AAAAAAAAAAI/AAAAAAAALKo/gkcVFXOt4zg/photo.jpg?sz=800',
            'gender'        => 'male',
            'locale'        => 'ID',
            //'placeLived'    => !empty($google_data['placeLived'])?$google_data['placeLived']:''
        );

        
        foreach($this->db->query("SELECT * FROM user WHERE oauth_id = '$oauth_id'")->result() as $usr){
            $id_user = $usr->id;
            $username = $usr->username;
        }

        $ip = $_SERVER['REMOTE_ADDR'];
        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
                
        $last_login = array(
            'id_user'   => $id_user,
            'ip'        => $ip,
            'host'      => $hostname
        );

        $uid = $google_data['email'];
        $wid = array('oauth_id' => $uid);

        if($this->db->query("SELECT * FROM user WHERE oauth_id = '$oauth_id'")->num_rows() > 0){
            $this->Query->update_data('user',$wid,$data_user_2);
            
            // Save LastLogin
            $this->Query->insert_data('last_login',$last_login);
        }else{
            $this->Query->insert_data('user',$data_user);
        }

        $data_session = array(
            'oauth_id'      => $oauth_id,
            'nickname'      => 'wildanmaulana',
            'first_name'    => 'Wildan',
            'last_name'     => 'Maulana',
            'email'         => 'wildaanmaulana@gmail.com',
            'oauth_provider'        => 'google',
            'img'           => 'https://lh6.googleusercontent.com/-q1PhA2MqUl0/AAAAAAAAAAI/AAAAAAAALKo/gkcVFXOt4zg/photo.jpg?sz=800',
            'id'            => $id_user,
            'logged_in'     => true,
            'username'      => !empty($username)?$username:''
        );
        
        
        $this->session->set_userdata($data_session);
        //$this->session->set_userdata($session_data);
        
        if($username == ""){
            redirect(base_url('set-username'));
        }else{
            redirect(base_url('dasbor'));
        }
    } 
}
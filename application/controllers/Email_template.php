<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email_template extends CI_Controller {

	public function __construct()
	{
		parent::__construct();		
        
        $this->load->library('email');
		
	}

	public function index()
	{
		redirect(base_url());
    }

    public function satu()
    {
        $this->load->view('template_email');
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
        $this->load->library('Google');
        $this->load->library('encryption');
        $this->load->library('email');
	}

	public function index()
	{
        if($this->session->userdata('logged_in') == true){
            redirect(base_url('dasbor'));
        }

        $data['googleurl']=$this->google->get_login_url();
		$this->load->view('landing',$data);
    } 
    
    public function auth()
    {
        $google_data=$this->google->validate();
        $oauth_id = $google_data['id'];

        date_default_timezone_set("Asia/Jakarta");

		$data_user = array(
            'oauth_id'      => $oauth_id,
            'nickname'      => !empty($google_data['nickname'])?$google_data['nickname']:'',
            'first_name'    => !empty($google_data['givenName'])?$google_data['givenName']:'',
            'last_name'     => !empty($google_data['familyName'])?$google_data['familyName']:'',
            'email'         => $google_data['email'],
            'oauth_provider'        => 'google',
            'img'           => !empty($google_data['profile_pic'])?$google_data['profile_pic']:'',
            'gender'        => !empty($google_data['gender'])?$google_data['gender']:'',
            'locale'        => !empty($google_data['lang'])?$google_data['lang']:'',
            //'placeLived'    => !empty($google_data['placeLived'])?$google_data['placeLived']:'',
            'date_registered' => date('Y-m-d H:i:s')
        );

        $data_user_2 = array(
            'oauth_id'      => $oauth_id,
            'email'         => $google_data['email'],
            'oauth_provider'        => 'google',
            'img'           => !empty($google_data['profile_pic'])?$google_data['profile_pic']:'',
            'gender'        => !empty($google_data['gender'])?$google_data['gender']:'',
            'locale'        => !empty($google_data['lang'])?$google_data['lang']:'',
            //'placeLived'    => !empty($google_data['placeLived'])?$google_data['placeLived']:''
        );

        
        foreach($this->db->query("SELECT * FROM user WHERE oauth_id = '$oauth_id'")->result() as $usr){
            $id_user = $usr->id;
            $username = $usr->username;
        }

        $ip = $_SERVER['REMOTE_ADDR'];
        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
                
        $last_login = array(
            'id_user'   => $id_user,
            'ip'        => $ip,
            'host'      => $hostname
        );

        $uid = $google_data['email'];
        $wid = array('oauth_id' => $uid);

        if($this->db->query("SELECT * FROM user WHERE oauth_id = '$oauth_id'")->num_rows() > 0){
            $this->Query->update_data('user',$wid,$data_user_2);
            
            // Save LastLogin
            $this->Query->insert_data('last_login',$last_login);
        }else{
            $this->Query->insert_data('user',$data_user);
        }

        $data_session = array(
            'oauth_id'      => $google_data['id'],
            'nickname'      => !empty($google_data['nickname'])?$google_data['nickname']:'',
            'first_name'    => !empty($google_data['givenName'])?$google_data['givenName']:'',
            'last_name'     => !empty($google_data['familyName'])?$google_data['familyName']:'',
            'email'         => $google_data['email'],
            'oauth_provider'        => 'google',
            'img'           => !empty($google_data['profile_pic'])?$google_data['profile_pic']:'',
            'id'            => $id_user,
            'logged_in'     => true,
            'username'      => !empty($username)?$username:''
        );
        
        
        $this->session->set_userdata($data_session);
        //$this->session->set_userdata($session_data);
        
        if($username == ""){
            redirect(base_url('set-username'));
        }else{
            redirect(base_url('dasbor'));
        }
    }


    public function not_found()
    {
        $this->load->view('not_found');
    }

    public function profile()
    {
        $id = $this->uri->segment(1);
        $id_user = $this->session->userdata('username');

        $usr0 = $this->db->query("SELECT * FROM user WHERE username = '$id_user'");
        if($usr0->num_rows() > 0){
            foreach($usr0->result() as $u){
                $usr = $u->username;
            }
        }else{
            $usr = "";
        }
        
        if($this->db->query("SELECT * FROM user WHERE username = '$id'")->num_rows() > 0){
            if($usr == $id){
                redirect(base_url());
            }else{
                $this->load->library('recaptcha');

                $data = array(
                    'widget' => $this->recaptcha->getWidget(),
                    'script' => $this->recaptcha->getScriptTag(),
                );

                $id = $this->uri->segment(1);
                $data['googleurl']=$this->google->get_login_url();

                foreach($this->db->query("SELECT * FROM user WHERE username = '$id'")->result() as $nm){
                    $data['username'] = $id;
                    $data['id']     = $nm->id;
                    $data['uid']    = $nm->oauth_id;
                    $data['img']    = $nm->img;
                    $data['fullname'] = $nm->first_name.' '.$nm->last_name;
                }

                $data['user'] = $id;
                $this->load->view('profile/send',$data);
            }
        }else{
            $this->load->view('not_found');
        }
    }

    public function message_send()
    {

    }

    public function thankyou()
    {
        if($this->session->flashdata('thankyou')){
            $data['messages'] = $this->session->flashdata('thankyou');
            $this->load->view('thankyou',$data);
        }else{
            redirect(base_url());
        }
    }

    /*
    public function encryptall()
    {
        foreach($this->db->get('pesan')->result() as $ps){
            $chatencrypt = array(
                'pesan' => $this->encryption->encrypt($ps->pesan),
                'tanggal'       => $ps->tanggal
            );
            $wchar = array('id' => $ps->id);
            $this->Query->update_data('pesan',$wchar,$chatencrypt);
        }
    }

    public function encrypt_email()
    {
        foreach($this->db->get('pesan')->result() as $ps){
            if($ps->email_balas == ""){
                $chatencrypt = array(
                    'email_balas' => '',
                    'tanggal' => $ps->tanggal
                );
            }else{
                $chatencrypt = array(
                    'email_balas' => $this->encryption->encrypt($ps->email_balas),
                    'tanggal'     => $ps->tanggal
                );
            }
            $wchar = array('id' => $ps->id);
            $this->Query->update_data('pesan',$wchar,$chatencrypt);
        }
    } */

    public function cariuser()
    {
        /*$phrase = "";
        if(isset($_GET['phrase'])) {
            $phrase = $_GET['phrase'];
        }

        $users = $this->db->get('user')->result();

        $found_users = array();
        foreach ($users as $key => $usr) {
            if ($phrase == "" || stristr($usr->username, $phrase) != false) {
                array_push($found_users	, $usr->username);
            }
        }

        $json = '[';
		foreach($found_users as $key => $usr) {
			$json .= '{"name": "' . $usr->username . '"}';
			if ($users !== end($found_users)) {
				$json .= ',';	
			}
		}
		$json .= ']';
		header('Content-Type: application/json');
        echo $json; */

        // Get search term
        $searchTerm = $_GET['term'];

        // Get matched data from skills table
        $query = $this->db->query("SELECT *,CONCAT_WS(' ',first_name,last_name) FROM user WHERE (username LIKE '%$searchTerm%' OR CONCAT_WS(' ',first_name,last_name) LIKE '%$searchTerm%') AND username IS NOT NULL");

        // Generate skills data array
        $skillData = array();
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data['id'] = $row->id;
                $data['value'] = $row->first_name.' '.$row->last_name;
                $data['label'] = '
                <a href="'.base_url($row->username).'" class="link-user">
                    <img src="'.$row->img.'" width="30" height="30" style="border-radius:100%"/>
                    <span class="ml-2">'
                        .$row->first_name.' '.$row->last_name.' 
                        <small> - '.$row->username.'</small>
                    </span>
                    
                </a>';
                array_push($skillData, $data);
            }
        }else{
            $data['label'] = '<small>Data tidak ditemukan..</small>';
            array_push($skillData, $data);
        }

        // Return results as json encoded array
        echo json_encode($skillData);

    }

    /*
    public function send_email_marketing()
    {
        foreach($this->db->query("SELECT * FROM email_list WHERE email_group = '7'")->result() as $usr){
            $this->email->initialize(array(
                'protocol' => 'smtp',
                'smtp_host' => 'mail.pesanrahasia.site',
                'smtp_user' => 'abdiel@pesanrahasia.site',
                'smtp_pass' => 'Gidinesia87!',
                'smtp_port' => 587,
                'crlf' => "\r\n",
                'newline' => "\r\n"
            ));
    
            $data['user'] = "";
            $email_konten = $this->load->view('template_email/marketing2',$data,TRUE);
            $to_email = $usr->email;
    
            $this->email->set_mailtype('html');
            $this->email->from('abdiel@pesanrahasia.site', 'Abdiel - Pesan Rahasia'); 
            $this->email->to($to_email);
            $this->email->subject('#Resolusi2019 - Muhasabah Diri di akhir 2018'); 
            $this->email->message($email_konten); 
            if ($this->email->send()) {
                echo 'Email sent - '.$to_email.'<br>';

                $data_email_send = array(
                    'email' => $usr->email,
                    'status'   => 'terkirim' 
                );
                $this->Query->insert_data('email_send',$data_email_send);

            }else{
                echo "email gagal kirim <br>";
                echo $this->email->print_debugger();
            }
        }
    } */

    
    public function tes_email_2(){
        /*
        $this->email->initialize(array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.pesanrahasia.site',
            'smtp_user' => 'hi@pesanrahasia.site',
            'smtp_pass' => 'Gidinesia87!',
            'smtp_port' => 587,
            'crlf' => "\r\n",
            'newline' => "\r\n"
        ));*/

        $this->email->initialize(array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.pesanrahasia.site',
            'smtp_user' => 'abdiel@pesanrahasia.site',
            'smtp_pass' => 'Gidinesia87!',
            'smtp_port' => 587,
            'crlf' => "\r\n",
            'newline' => "\r\n"
        ));

        $data['user'] = "";
        $email_konten = $this->load->view('template_email/marketing2',$data,TRUE);
        $to_email = 'wildaanmaulana@gmail.com';

        $this->email->set_mailtype('html');
        $this->email->from('abdiel@pesanrahasia.site', 'Abdiel - Pesan Rahasia'); 
        $this->email->to($to_email);
        $this->email->subject('#Resolusi2019 - Muhasabah Diri di akhir 2018'); 
        $this->email->message($email_konten); 
        if ($this->email->send()) {
            echo 'Email sent - '.$to_email.'<br>';
        }else{
            echo "email gagal kirim <br>";
            echo $this->email->print_debugger();
        }

        //redirect(base_url('message-send'));
    }

    public function sendgrid_test()
    {
        $this->load->library('Sendgrid_mail');
        // Initialize (not necessary if set in config)
        $this->sendgrid_mail->initialize(array(
            'api_user'   => 'pesanrahasia',
            'api_key'    => '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',
            'api_format' => 'json')
        );

        // Send email
        $result = $this->sendgrid_mail->send('hi@pesanrahasia.site', 'Welcome to Oz!', 'You may see the wizard now.', NULL, 'work.wildan@gmail.com');
    }
    
}

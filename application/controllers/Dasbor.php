<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dasbor extends CI_Controller {

	public function __construct()
	{
		parent::__construct();		
		
		if($this->session->userdata('logged_in') == false){
			redirect(base_url());
		}

		$this->load->library('encryption');
	}

	public function index()
	{
		$id = $this->session->userdata('oauth_id');

		$id_user = $this->session->userdata('id');
		$data['id'] = $id_user;

		$d_usr = $this->db->query("SELECT * FROM user WHERE oauth_id = '$id'")->result();
		foreach($d_usr as $usr){
			$username = $usr->username;
			$first_name = $usr->first_name;
			$last_name = $usr->last_name;
		}
		
		$username = !empty($username)?$username:'';
		$data['username'] = $username;
		$data['first_name'] = $first_name;
		$data['last_name'] = $last_name;
		
		if($username == ""){
			redirect(base_url('set-username'));
		}else{
			$this->load->view('dasbor/index',$data);
		}
	}

	public function next()
	{
		if($this->session->userdata('logged_in') == true){

			$id = $this->session->userdata('oauth_id');

			$d_usr = $this->db->query("SELECT * FROM user WHERE oauth_id = '$id'")->result();
			foreach($d_usr as $usr){
				$username = $usr->username;
			}

			$username = !empty($username)?$username:'';

			if($username == ""){
				$this->load->view('dasbor/next');
			}else{
				redirect(base_url());
			}

		}else{
			redirect(base_url());
		}
	}

	public function cekusername()
	{
		if(isset($_POST['username'])){
			$username = $_POST['username'];
			$user_session = $this->session->userdata('oauth_id');

			if($this->db->query("SELECT * FROM user WHERE oauth_id = '$user_session'")->num_rows() == 0){
				$user_sess = "";
			}else{
				foreach($this->db->query("SELECT * FROM user WHERE oauth_id = '$user_session'")->result() as $us){
					$user_sess = $us->username;
				}
			}

			if($username == $user_sess){
				$val_email = 0;
			}else{
				$val_email = $this->db->query("SELECT * FROM user WHERE username = '$username'")->num_rows();
			}
			

			/*
			if(!empty($email)){

				if(filter_var($email, FILTER_VALIDATE_EMAIL) === false){
					echo ' <span style="color:red">Sorry, email belum valid</span>';
				}else{
					echo '';
				}

			}*/
			if($val_email > 0){
				$result = false;
			}else{
				$result = true;
			}
			header('Content-Type: application/json');
			echo json_encode($result);
		}
	}

	public function changeUsername()
	{
		$uid = $this->session->userdata('oauth_id');

		if($this->session->userdata('logged_in') == true){
			$getProfile = $this->db->query("SELECT * FROM user WHERE oauth_id = '$uid'");
			foreach($getProfile->result() as $p ){
				$data['first_name']	= $p->first_name;
				$data['last_name']	= $p->last_name;
				$data['username']	= $p->username;
			}
			$this->load->view('dasbor/ubahusername',$data);
		}else{
			redirect(base_url());
		}
	}

	public function ubah_profil()
	{
		$getProfile = $this->db->query("SELECT * FROM pesan WHERE id = '$id_pesan' AND penerima = '$id_user'");
	}

	public function nextstep()
	{
		$id = $this->session->userdata('oauth_id');
		$wid = array('oauth_id' => $id);
		$usr = array('username'	=> $this->input->post('username'));

		$username = $this->input->post('username');

		$user_session = $this->session->userdata('oauth_id');

		if($this->db->query("SELECT * FROM user WHERE oauth_id = '$user_session'")->num_rows() == 0){
			$user_sess = "";
		}else{
			foreach($this->db->query("SELECT * FROM user WHERE oauth_id = '$user_session'")->result() as $us){
				$user_sess = $us->username;
			}
		}

		if($user_sess == $username){
			$val_email = 0;
		}else{
			$val_email = $this->db->query("SELECT * FROM user WHERE username = '$username'")->num_rows();
		}

		if($val_email > 0){
			$this->session->set_flashdata('notif_username', 'Username telah digunakan!');
			redirect(base_url('ubah-username'));
		}else{
			$this->Query->update_data('user',$wid,$usr);
			redirect(base_url('dasbor'));
		}
	}

	public function ubahprofil()
	{
		$id = $this->session->userdata('oauth_id');
		$username = $this->input->post('username');

		$wid = array('oauth_id' => $id);
		$usr = array(
			'username'		=> $this->input->post('username'),
			'first_name'	=> $this->input->post('first_name'),
			'last_name'		=> $this->input->post('last_name')
		);

		if($this->db->query("SELECT * FROM user WHERE oauth_id = '$id'")->num_rows() == 0){
			$user_sess = "";
		}else{
			foreach($this->db->query("SELECT * FROM user WHERE oauth_id = '$id'")->result() as $us){
				$user_sess = $us->username;
			}
		}

		if($user_sess == $username){
			$val_email = 0;
		}else{
			$val_email = $this->db->query("SELECT * FROM user WHERE username = '$username'")->num_rows();
		}
		
		if($val_email > 0){
			$this->session->set_flashdata('notif_username', 'Username telah digunakan!');
			redirect(base_url('ubah-username'));
		}else{
			$this->session->set_flashdata('notif_username', 'Profil kamu berhasil diubah');
			$this->Query->update_data('user',$wid,$usr);
			redirect(base_url('dasbor'));
		}
	}

	public function logout()
    {
        $this->session->sess_destroy();
		redirect(base_url());
	}
	
}

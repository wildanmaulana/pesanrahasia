<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
        $this->load->library('Google');
	}

	public function index()
	{
        if($this->session->userdata('logged_in') == true){
            redirect(base_url('dasbor'));
        }

        $data['googleurl']=$this->google->get_login_url();
		$this->load->view('landing',$data);
    } 
    
    public function auth()
    {
        $google_data=$this->google->validate();
        $oauth_id = $google_data['id'];

        date_default_timezone_set("Asia/Jakarta");

		$data_user = array(
            'oauth_id'      => $oauth_id,
            'nickname'      => !empty($google_data['nickname'])?$google_data['nickname']:'',
            'first_name'    => !empty($google_data['givenName'])?$google_data['givenName']:'',
            'last_name'     => !empty($google_data['familyName'])?$google_data['familyName']:'',
            'email'         => $google_data['email'],
            'oauth_provider'        => 'google',
            'img'           => !empty($google_data['profile_pic'])?$google_data['profile_pic']:'',
            'gender'        => !empty($google_data['gender'])?$google_data['gender']:'',
            'locale'        => !empty($google_data['lang'])?$google_data['lang']:'',
            //'placeLived'    => !empty($google_data['placeLived'])?$google_data['placeLived']:'',
            'date_registered' => date('Y-m-d H:i:s')
        );

        $data_user_2 = array(
            'oauth_id'      => $oauth_id,
            'email'         => $google_data['email'],
            'oauth_provider'        => 'google',
            'img'           => !empty($google_data['profile_pic'])?$google_data['profile_pic']:'',
            'gender'        => !empty($google_data['gender'])?$google_data['gender']:'',
            'locale'        => !empty($google_data['lang'])?$google_data['lang']:'',
            //'placeLived'    => !empty($google_data['placeLived'])?$google_data['placeLived']:''
        );

        
        foreach($this->db->query("SELECT * FROM user WHERE oauth_id = '$oauth_id'")->result() as $usr){
            $id_user = $usr->id;
            $username = $usr->username;
        }

        $ip = $_SERVER['REMOTE_ADDR'];
        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
                
        $last_login = array(
            'id_user'   => $id_user,
            'ip'        => $ip,
            'host'      => $hostname
        );

        $uid = $google_data['email'];
        $wid = array('oauth_id' => $uid);

        if($this->db->query("SELECT * FROM user WHERE oauth_id = '$oauth_id'")->num_rows() > 0){
            $this->Query->update_data('user',$wid,$data_user_2);
            
            // Save LastLogin
            $this->Query->insert_data('last_login',$last_login);
        }else{
            $this->Query->insert_data('user',$data_user);
        }

        $data_session = array(
            'oauth_id'      => $google_data['id'],
            'nickname'      => !empty($google_data['nickname'])?$google_data['nickname']:'',
            'first_name'    => !empty($google_data['givenName'])?$google_data['givenName']:'',
            'last_name'     => !empty($google_data['familyName'])?$google_data['familyName']:'',
            'email'         => $google_data['email'],
            'oauth_provider'        => 'google',
            'img'           => !empty($google_data['profile_pic'])?$google_data['profile_pic']:'',
            'id'            => $id_user,
            'logged_in'     => true,
            'username'      => !empty($username)?$username:''
        );
        
        
        $this->session->set_userdata($data_session);
        //$this->session->set_userdata($session_data);
        
        if($username == ""){
            redirect(base_url('set-username'));
        }else{
            redirect(base_url('dasbor'));
        }
    }

    public function not_found()
    {
        $this->load->view('not_found');
    }

    public function profile()
    {
        $id = $this->uri->segment(1);
        $id_user = $this->session->userdata('username');

        $usr0 = $this->db->query("SELECT * FROM user WHERE username = '$id_user'");
        if($usr0->num_rows() > 0){
            foreach($usr0->result() as $u){
                $usr = $u->username;
            }
        }else{
            $usr = "";
        }
        
        if($this->db->query("SELECT * FROM user WHERE username = '$id'")->num_rows() > 0){
            if($usr == $id){
                redirect(base_url());
            }else{
                $this->load->library('recaptcha');

                $data = array(
                    'widget' => $this->recaptcha->getWidget(),
                    'script' => $this->recaptcha->getScriptTag(),
                );

                $id = $this->uri->segment(1);
                $data['googleurl']=$this->google->get_login_url();

                foreach($this->db->query("SELECT * FROM user WHERE username = '$id'")->result() as $nm){
                    $data['username'] = $id;
                    $data['id']     = $nm->id;
                    $data['uid']    = $nm->oauth_id;
                    $data['img']    = $nm->img;
                    $data['fullname'] = $nm->first_name.' '.$nm->last_name;
                }

                $data['user'] = $id;
                $this->load->view('profile/send',$data);
            }
        }else{
            $this->load->view('not_found');
        }
    }

    public function message_send()
    {

    }

    public function thankyou()
    {
        if($this->session->flashdata('thankyou')){
            $data['messages'] = $this->session->flashdata('thankyou');
            $this->load->view('thankyou',$data);
        }else{
            redirect(base_url());
        }
    }

}

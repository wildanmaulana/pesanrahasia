<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends CI_Controller {

	public function __construct()
	{
		parent::__construct();		
        
        $this->load->library('email');
		/*
		if($this->session->userdata('logged_in') == false){
			redirect(base_url('login'));
		}
		if($this->session->userdata('role') == 1){
			redirect(base_url('admin'));
		} */
		$this->load->library('encryption');
	}

	public function index()
	{
		redirect(base_url());
    }
    
    public function balas_pesan()
    {
        $id_user = $this->session->userdata('id');
        $id_pesan = $this->input->post('id_pesan');

        $getMes = $this->db->query("SELECT * FROM pesan WHERE id = '$id_pesan' AND penerima = '$id_user'");

        foreach($getMes->result() as $tbl){
            if($tbl->balas == false){
                redirect(base_url());
            }
        }

        if($getMes->num_rows() == 0){
            redirect(base_url());
        } else {
            $data_balasan = array(
                'id_pesan'  => $id_pesan,
                'id_user'   => $id_user, 
                'pesan_balasan' => $this->input->post('pesan')
            );
    
            $this->Query->insert_data('pesan_balasan',$data_balasan);
            $this->session->set_flashdata('pesan_terkirim', 'Pesan balasan anda terkirim');
            
            // Kirim ke email
            $this->email->initialize(array(
                'protocol' => 'smtp',
                'smtp_host' => 'mail.pesanrahasia.site',
                'smtp_user' => 'hi@pesanrahasia.site',
                'smtp_pass' => 'Gidinesia87!',
                'smtp_port' => 587,
                'crlf' => "\r\n",
                'newline' => "\r\n"
            ));
            
            $data['pesan'] = $this->input->post('pesan');
            $email_konten = $this->load->view('email_balas',$data,TRUE);
            $to_email = $this->input->post('email');

            $this->email->set_mailtype('html');
            $this->email->from('hi@pesanrahasia.site', 'Pesan Rahasia'); 
            $this->email->to($to_email);
            $this->email->subject('Horee.. Pesan kamu dibalas...'); 
            $this->email->message($email_konten); 
            if ($this->email->send()) {
                echo 'Email sent.';
            }
            redirect(base_url('chat/'.$id_pesan));
        }
    }

	public function send_to_user()
	{
        if(isset($_POST['penerima'])){
            $this->load->library('form_validation');
            $this->load->library('recaptcha');
            $recaptcha = $this->input->post('g-recaptcha-response');
            if (!empty($recaptcha)) {
                $response = $this->recaptcha->verifyResponse($recaptcha);
                if (isset($response['success']) and $response['success'] === true) {
                    echo "You got it!";
                }else{
                    $this->session->set_flashdata('captcha', 'anda harus memverifikasi ReCaptcha');
                    echo "<script>window.history.back();</script>";
                }
            }
        
            $jenis0 = $this->input->post('jenis');

            if($jenis0 == ""){
                $jenis = 1;
            }else{
                $jenis = $jenis0;
            }

            if($this->input->post('anonim') == "yes") {
                $pengirim = "Anonim";
            }else {
                $pengirim = $this->input->post('pengirim_name');
            }

            if($this->input->post('balas') == "yes") {
                $balas = true;
                $email_pengirim = $this->input->post('email_pengirim');
            }else{
                $balas = false;
                $email_pengirim = "";
            }

            $pnrm = $this->input->post('penerima');
            foreach($this->db->query("SELECT * FROM user WHERE id ='$pnrm'")->result() as $em){
                $to_email = $em->email;
                $user = $em->first_name.' '.$em->last_name;
            }

            $data_pesan = array(
                'kategori'  => $jenis,
                'pengirim'  => $pengirim,
                'penerima'  => $this->input->post('penerima'),
                'balas'     => $balas,
                'email_balas' => $email_pengirim,
                'pesan'     => $this->encryption->encrypt($this->input->post('pesan'))
            );

            $this->Query->insert_data('pesan',$data_pesan);
            
            $data['url_send'] = $this->input->post('url_profil');
            $this->load->view('message_send',$data);
            $this->session->set_flashdata('send_true', 'true');
            $this->session->set_flashdata('url_send', $this->input->post('url_profil'));
            
            // Send to email
            $this->email->initialize(array(
                'protocol' => 'smtp',
                'smtp_host' => 'mail.pesanrahasia.site',
                'smtp_user' => 'hi@pesanrahasia.site',
                'smtp_pass' => 'Gidinesia87!',
                'smtp_port' => 587,
                'crlf' => "\r\n",
                'newline' => "\r\n"
            ));

            $data['user'] = $user;
            $email_konten = $this->load->view('template_email',$data,TRUE);

            $this->email->set_mailtype('html');
            $this->email->from('hi@pesanrahasia.site', 'Pesan Rahasia'); 
            $this->email->to($to_email);
            $this->email->subject('Anda mendapatkan pesan rahasia lhoo...'); 
            $this->email->message($email_konten); 
            if ($this->email->send()) {
                echo 'Email sent.';
            }

            redirect(base_url('message-send'));
        } else {
            redirect(base_url());
        }
    }
    
    public function message_send()
    {
        if($this->session->flashdata('send_true')){
            $data['url_send'] = $this->session->flashdata('url_profil');
            $this->load->view('message_send',$data);
        }else{
            redirect(base_url());
        }
    }

    public function read()
    {
        $id = $this->uri->segment(2);
        $uid = $this->session->userdata('id');
        
        $getMes = $this->db->query("SELECT *,p.id as id_pesan FROM pesan p LEFT JOIN kategori_pesan k ON(p.kategori = k.id) WHERE p.id = '$id'");
        // Chat Validation
        if($getMes->num_rows() > 0){
            if($this->db->query("SELECT * FROM pesan WHERE id = '$id' AND penerima = '$uid'")->num_rows() == 0) {
                redirect(base_url());
            } else {
                $data['getMess'] = $getMes;
                $this->load->view('dasbor/seeMessages',$data);
            }
        } else {
            redirect(base_url());
        }

        // Cek apakah minta dibalas 
        if($this->uri->segment(3) == "reply"){
            foreach($getMes->result() as $tbl){
                if($tbl->balas == false){
                    redirect(base_url('chat/'.$this->uri->segment(2)));
                }
            }
        }
    }

    public function feedback()
    {
        $this->load->library('recaptcha');
        $recaptcha = $this->input->post('g-recaptcha-response');
        if (!empty($recaptcha)) {
            $response = $this->recaptcha->verifyResponse($recaptcha);
            if (isset($response['success']) and $response['success'] === true) {
                echo "You got it!";
            }
        }

        $data = array(
            'widget' => $this->recaptcha->getWidget(),
            'script' => $this->recaptcha->getScriptTag(),
        );
        $this->load->view('feedback',$data);
        
    }

    public function send_feedback()
    {
        if($this->input->post('anonim') == "yes") {
            $pengirim = "Anonim";
        }else {
            $pengirim = $this->input->post('pengirim_name');
        }

        $data_feedback = array(
            'pengirim'  => $pengirim,
            'email'     => $this->input->post('email'),
            'pesan'     => $this->input->post('pesan')
        );

        $this->Query->insert_data('feedback',$data_feedback);
        $this->session->set_flashdata('thankyou', 'Terima Kasih, feedback anda telah kami terima.');
        
        // Send to email
        $this->email->initialize(array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.pesanrahasia.site',
            'smtp_user' => 'hi@pesanrahasia.site',
            'smtp_pass' => 'Gidinesia87!',
            'smtp_port' => 587,
            'crlf' => "\r\n",
            'newline' => "\r\n"
        ));

        $email_konten = $this->input->post('pesan');
        $to_email = 'developer@pesanrahasia.site, work.wildan@gmail.com';

        $this->email->set_mailtype('html');
        $this->email->from('hi@pesanrahasia.site', 'DEV.PesanRahasia'); 
        $this->email->to($to_email);
        $this->email->subject('Ada Feedback baru...'); 
        $this->email->message($email_konten); 
        if ($this->email->send()) {
            echo 'Email sent.';
        }

        $email_konten2 = "Terima Kasih telah memberi feedback, kami akan segera menanggapi feedback anda secepat mungkin :D  -Tim Developer PesanRahasia-";
        $to_email2 = $this->input->post('email');

        $this->email->set_mailtype('html');
        $this->email->from('hi@pesanrahasia.site', 'Developer PesanRahasia'); 
        $this->email->to($to_email2);
        $this->email->subject('Terima Kasih Feedback anda...'); 
        $this->email->message($email_konten2); 
        if ($this->email->send()) {
            echo 'Email sent.';
        }

        redirect(base_url('thank-you'));
    }

    

}
